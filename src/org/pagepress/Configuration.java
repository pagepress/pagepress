/*
   Copyright 2013 PagePress Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.pagepress;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {
    public static Properties prop;

    public static void load() {
        Properties prop = new Properties();

        try {
            prop.load(new FileInputStream("pagepress.properties"));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void create() {
        Properties prop = new Properties();
        try {
            prop.setProperty("server", "localhost");
            prop.setProperty("port", "21");
            prop.setProperty("username", "user");
            prop.setProperty("password", "pass");
            prop.setProperty("applied-theme", "default");
            prop.store(new FileOutputStream("pagepress.properties"), null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return prop.getProperty(key);
    }
}