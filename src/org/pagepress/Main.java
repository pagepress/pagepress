/*   
   Copyright 2013 PagePress Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.pagepress;

import org.pagepress.posts.Post;
import org.pagepress.themes.Theme;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class Main {

    public static ArrayList<Post> importedPosts;
    public static ArrayList<Theme> importedThemes;

    public static void main(String[] args) throws Exception {

        loadConfig();
        loadTheme();
        loadPosts();

    }

    public static void loadConfig() throws IOException {
        File configFile = new File("pagepress.properties");
        if (!(configFile.exists())) {
            Configuration.create();
            System.out.println("Created default configuration, please adjust these settings.");
            System.exit(0);
        } else {
            Configuration.load();
        }
    }

    public static void loadTheme() throws ParserConfigurationException, SAXException, IOException {
        File themes_directory = new File("themes/");
        DocumentBuilderFactory dbTFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dTBuilder = dbTFactory.newDocumentBuilder();
        if (!(themes_directory.exists()))
            themes_directory.mkdirs();
        if (!(themes_directory.isDirectory())) {
            System.out.println("Error! Can not create posts/ directory. File \"posts\" already exists!");
            return;
        }
        File[] themeList = themes_directory.listFiles();
        for (File aThemeList : themeList) {
            if (!(aThemeList.getName().endsWith(".xml")))
                continue;
            Document doc = dTBuilder.parse(aThemeList);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("theme");
            for (int j = 0; j < nList.getLength(); j++) {
                Node nNode = nList.item(j);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getElementsByTagName("title").item(0).getTextContent();
                    String templateFN = eElement.getElementsByTagName("template").item(0).getTextContent();
                    String author = eElement.getElementsByTagName("author").item(0).getTextContent();
                    File template = new File(templateFN);
                    importedThemes.add(new Theme(name, template, author));
                }
            }
        }
        System.out.println("Themes have been imported!");
    }

    public static void loadPosts() throws IOException, ParserConfigurationException, SAXException {
        File post_directory = new File("posts/");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        if (!(post_directory.exists()))
            assert post_directory.mkdirs();
        if (!(post_directory.isDirectory())) {
            System.out.println("Error! Can not create posts/ directory. File \"posts\" already exists!");
            return;
        }
        File[] postList = post_directory.listFiles();
        for (File aPostList : postList) {
            if (!(aPostList.getName().endsWith(".xml")))
                continue;
            Document doc = dBuilder.parse(aPostList);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("post");
            for (int j = 0; j < nList.getLength(); j++) {
                Node nNode = nList.item(j);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    Date date = (Date) eElement.getElementsByTagName("date");
                    String title = eElement.getElementsByTagName("title").item(0).getTextContent();
                    String author = eElement.getElementsByTagName("author").item(0).getTextContent();
                    String[] content = eElement.getElementsByTagName("content").item(0).getTextContent().split(" ");
                    importedPosts.add(new Post(date, title, author, content));
                }
            }
        }
        System.out.println("Post have been imported!");
    }

}
