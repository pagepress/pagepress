/*   
   Copyright 2013 PagePress Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package org.pagepress.posts;

import java.util.Date;

public class Post {

    private Date date;
    private String title;
    private String author;
    private String[] content;

    public Post(Date date, String title, String author, String[] content) {
        this.date = date;
        this.title = title;
        this.author = author;
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String[] getContent() {
        return content;
    }

}
